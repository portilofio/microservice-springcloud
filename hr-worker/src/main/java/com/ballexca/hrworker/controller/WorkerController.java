package com.ballexca.hrworker.controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ballexca.hrworker.entities.Worker;
import com.ballexca.hrworker.repositories.WorkerRepository;

@RestController
@RequestMapping("/workers")
public class WorkerController {

	static Logger logger = LoggerFactory.getLogger(WorkerController.class);
	
	@Autowired
	private Environment environment;
	
	private WorkerRepository workerRepository;

	@Autowired
	public WorkerController(WorkerRepository workerRepository) {
		this.workerRepository = workerRepository;
		
	}
	
	@GetMapping
	@ResponseStatus(code = HttpStatus.OK)
	public List<Worker> findAll() {
		return this.workerRepository.findAll();
	}
	
	@GetMapping("/{id}")
	@ResponseStatus(code = HttpStatus.OK)
	public ResponseEntity<Worker> findById(@PathVariable Long id) {
		logger.info("PORT = " + environment.getProperty("local.server.port"));
		
		
		Optional<Worker> optional = this.workerRepository.findById(id);
		
		return optional
				.map(worker -> ResponseEntity.ok(worker))
					.orElse(ResponseEntity.notFound().build());
	}
	
}
