package com.ballexca.hrpayroll.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ballexca.hrpayroll.entities.Payment;
import com.ballexca.hrpayroll.entities.Worker;
import com.ballexca.hrpayroll.feignclients.WorkerFeignClient;

@Service
public class PaymentService {
	
	private WorkerFeignClient workerFeignClient;

	@Autowired
	public PaymentService(WorkerFeignClient workerFeignClient) {
		this.workerFeignClient = workerFeignClient;
	}
	
	public Payment getPayment(long id, int days) {
		ResponseEntity<Worker> responseEntity = workerFeignClient.findById(id);
		Worker worker = responseEntity.getBody();
		
		return new Payment(worker.getName(), worker.getDailyIncome(), days);
	}
}
