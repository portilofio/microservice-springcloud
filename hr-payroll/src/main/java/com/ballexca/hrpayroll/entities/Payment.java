package com.ballexca.hrpayroll.entities;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Payment implements Serializable {

	private String name;
	private Double dailyIncome;
	private Integer days;

	public Double getTotal() {
		return (days * dailyIncome);
	}
}
