package com.ballexca.hrpayroll.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ballexca.hrpayroll.entities.Payment;
import com.ballexca.hrpayroll.services.PaymentService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@RestController
@RequestMapping("/payments")
public class PaymentController {

	private PaymentService paymentService;

	@Autowired
	public PaymentController(PaymentService paymentService) {
		this.paymentService = paymentService;
	}
	
	@HystrixCommand(fallbackMethod = "getPaymentAlternative")
	@GetMapping("/{workerId}/days/{days}")
	@ResponseStatus(code = HttpStatus.OK)
	public Payment getPayment(@PathVariable Long workerId, @PathVariable Integer days) {
		return paymentService.getPayment(workerId, days);
	}
	
	public Payment getPaymentAlternative(@PathVariable Long workerId, @PathVariable Integer days) {
		return new Payment("Eduardo", 300.0, days);
	}
}
