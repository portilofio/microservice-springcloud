package com.ballexca.hrworker.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ballexca.hrworker.entities.Worker;

public interface WorkerRepository extends JpaRepository<Worker, Long> {

}
